from django.shortcuts import render,get_object_or_404,redirect
from books.models import Book
from books.form import BookForm
# Create your views here.


def book_list(request):
    book  = Book.objects.all()
    context = {
        "object_list" : book
    }
    return render(request, "books/book_list.html", context)

def book_view(request,pk):
    book = get_object_or_404(Book, pk=pk)
    context = {
        "object" : book
    }
    return render(request, "books/book_detail.html", context)

def book_create(request):
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('book_list')
    return render(request, "books/book_form.html",{ 'form' : form })

def book_update(request,pk):
    book = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect('book_list')
    
    return render(request, "books/book_form.html", {'form':form})

def book_delete(request,pk):
    book = get_object_or_404(Book, pk=pk)
    if request.method=='POST':
        book.delete()
        return redirect('book_list')
    return render(request, "books/book_confirm_delete.html", {'object':book})
   

